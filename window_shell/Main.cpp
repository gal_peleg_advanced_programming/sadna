#include <iostream>
#include <vector>
#include "Helper.h"
#include "Shell.h"

using std::cout;
using std::cin;
using std::endl;

int main()
{
	Shell shell = Shell(); // the interface initialization
	vector<string> words;
	string input;
	while (true)
	{
		try
		{
			cout << ">>";
			std::getline(cin, input);
			words = Helper::get_words(input);
			//find the key word of every function
			if (words[0] == "pwd")
			{
				cout << shell.pwd() << endl;;
			}
			else if (words[0] == "cd")
			{
				if (words.size() > 1)
				{
					shell.cd(words[1]);
				}
			}
			else if (words[0] == "create")
			{
				if (words.size() > 1)
				{
					shell.create(words[1]);
				}
			}
			else if (words[0] == "ls")
			{
				words = *(shell.ls());
				for (int i = 0; i < words.size(); i++)
				{
					cout << words[i] << endl;
				}
			}
			else if (words[0] == "secret")
			{
				cout << shell.secret() << endl;;
			}
			else
			{
				shell.exe(words[0]);
			}
		}
		catch (const std::exception&)
		{
			cout << "Sorry something went wrong" << endl;
		}
	}
}