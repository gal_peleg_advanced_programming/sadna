#include "Shell.h"

/*
this function return a string of the path to the application folder
input: none
output: path 
*/
string Shell::pwd()
{
	TCHAR buffer[BUFSIZE];
	DWORD path;
	path = GetCurrentDirectory(BUFSIZE, buffer);
	if (path == 0)
	{
		throw EXCEPTION_EXECUTE_HANDLER;
	}
	return buffer;
}
/*
this function changes the path of the application 
input: dest path
output: none
*/
void Shell::cd(string path)
{
	DWORD check = SetCurrentDirectory(path.c_str());
	if (check == 0)
	{
		throw EXCEPTION_EXECUTE_HANDLER;
	}
}
/*
this function creates a file
input: the file name 
output: none
*/
void Shell::create(string name)
{
	HANDLE check = CreateFile(name.c_str(), GENERIC_ALL, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
	if (check == 0)
	{
		throw EXCEPTION_EXECUTE_HANDLER;
	}

}
/*
this function returns all the names of the files in the directory
input: none
output: string vector of the files
*/
vector<string>* Shell::ls()
{
	vector<string>* files = new vector<string>();
	HANDLE hFind = INVALID_HANDLE_VALUE;
	WIN32_FIND_DATA ffd;
	TCHAR path[BUFSIZE];
	GetCurrentDirectory(BUFSIZE, path);
	StringCchCat(path, MAX_PATH, TEXT("\\*"));
	hFind = FindFirstFile(path, &ffd);
	if (hFind == INVALID_HANDLE_VALUE)
	{
		throw EXCEPTION_EXECUTE_HANDLER;
	}
	do
	{
		files->push_back(ffd.cFileName);
	} 
	while (FindNextFile(hFind, &ffd));
	return files;
	//the dots are files without premitions
	//hidden files or directory
}
/*
this function use a dll and return the dll function return value
intput: none
output: the mining of life
*/
int Shell::secret()
{
	HINSTANCE dllHandle = NULL;
	FARPROC function = NULL;
	dllHandle = LoadLibrary("Secret.dll");
	if (dllHandle == INVALID_HANDLE_VALUE)
	{
		throw EXCEPTION_EXECUTE_HANDLER;
	}
	function = GetProcAddress(dllHandle,"TheAnswerToLifeTheUniverseAndEverything");
	return (function());
}
/*
this function run exe file
input: the exe file name
output: none
*/
void Shell::exe(string name)
{
	vector<string>* files = ls();
	bool found = false;
	for (int i = 0; i < files->size(); i++)
	{
		if (name == (*files)[i])
		{
			found = true;
			break;
		}
	}
	if (found)
	{
		WinExec(name.c_str(), SW_SHOWNORMAL);
	}
}