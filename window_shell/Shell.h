#pragma once

#include <iostream>
#include <Windows.h>
#include <string>
#include <strsafe.h>
#include <vector>

#define BUFSIZE MAX_PATH // max path size 

using std::vector;
using std::string;
using std::cout;
using std::endl;

class Shell //this interface has the winapi function for this program
{
public:
	string pwd();
	void cd(string path);
	void create(string name);
	vector<string>* ls();
	int secret();
	void exe(string name);
};